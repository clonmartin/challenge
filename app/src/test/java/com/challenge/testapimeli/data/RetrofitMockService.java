package com.challenge.testapimeli.data;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.data.response.ApiResponseDescription;
import com.challenge.testapimeli.data.response.ApiResponseItem;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;

/**
 * Mock server to test. See <a
 * href="https://github.com/square/retrofit/blob/master/samples/src/main/java/com/example/retrofit/SimpleMockService.java">
 * here</a>
 */
public class RetrofitMockService {

  /** A mock implementation of the {@link ApiService} API interface. */
  public static final class MockApiService implements ApiService {
    private final BehaviorDelegate<ApiService> delegate;
    private ApiResponseDescription responseDescription;
    private ApiResponseItem responseItem;
    private ApiResponseSearch responseSearch;
    private ApiResponseCategories responseCategories;
    private final boolean ErrorMode;
    public String jsonError;

    public MockApiService(BehaviorDelegate<ApiService> delegate, boolean errorMode) {
      this.delegate = delegate;
      ErrorMode = errorMode;
      responseCategories = new ApiResponseCategories();
    }

    Object jsonToSerializedClass(String string, Class classType) {
      Gson gson = new GsonBuilder().create();
      return gson.fromJson(string, classType);
    }

    public void addCategories(String jsonStringTest) {
      responseCategories =
          (ApiResponseCategories)
              jsonToSerializedClass(jsonStringTest, ApiResponseCategories.class);
    }

    public void addDescription(String jsonStringTest) {
      responseDescription =
          (ApiResponseDescription)
              jsonToSerializedClass(jsonStringTest, ApiResponseDescription.class);
    }

    public void addItem(String jsonStringTest) {
      responseItem = (ApiResponseItem) jsonToSerializedClass(jsonStringTest, ApiResponseItem.class);
    }

    public void addSearch(String jsonStringTest) {
      responseSearch =
          (ApiResponseSearch) jsonToSerializedClass(jsonStringTest, ApiResponseSearch.class);
    }

    @Override
    public Call<ApiResponseCategories> callCategories() {
      if (ErrorMode) {
        try {
          Response<Object> response =
              Response.error(
                  404, ResponseBody.create(jsonError, MediaType.parse("application/json")));
          return delegate.returning(Calls.response(response)).callCategories();
        } catch (JsonParseException e) {
          return Calls.failure(e);
        }
      } else {
        return delegate.returningResponse(responseCategories).callCategories();
      }
    }

    @Override
    public Call<ApiResponseSearch> callSearchProduct(
        @NonNull String searchProduct, int limit, int offset) {
      return delegate
          .returningResponse(responseSearch)
          .callSearchProduct(searchProduct, limit, offset);
    }

    @Override
    public Call<ApiResponseItem> callItemAttributes(
        @NonNull String ids, @NonNull String attributes) {
      return delegate.returningResponse(responseItem).callItemAttributes(ids, attributes);
    }

    @Override
    public Call<ApiResponseDescription> callDescription(@NonNull String idItem) {
      return delegate.returningResponse(responseDescription).callDescription(idItem);
    }
  }
}
