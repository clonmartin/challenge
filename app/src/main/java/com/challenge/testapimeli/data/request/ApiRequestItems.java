package com.challenge.testapimeli.data.request;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/** Implementation class to create parameters to get items from the api */
public class ApiRequestItems implements ApiRequest {
  @NonNull private String id;
  @NonNull private String attributes;

  public ApiRequestItems(@NonNull String id, @Nullable String attributes) {
    this.id = id;
    if (attributes != null) {
      this.attributes = attributes;
    } else {
      this.attributes = "";
    }
  }

  @NonNull
  public String getId() {
    return id;
  }

  public void setId(@NonNull String id) {
    this.id = id;
  }

  @NonNull
  public String getAttributes() {
    return attributes;
  }

  public void setAttributes(@NonNull String attributes) {
    this.attributes = attributes;
  }
}
