package com.challenge.testapimeli.ui.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.challenge.testapimeli.data.api.ApiManagerCategories;
import com.challenge.testapimeli.data.response.ApiResponse;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

/**
 * This view model uses api manager to send request and get responses setting categoryList from
 * callback
 */
@HiltViewModel
public class MainViewModel extends ViewModel {
  private final LiveData<ApiResponse> categoryList;
  private final ApiManagerCategories apiManagerCategories;

  /**
   * Set api manager to call categories
   *
   * @param apiManagerCategories
   */
  @Inject
  public MainViewModel(ApiManagerCategories apiManagerCategories) {
    this.apiManagerCategories = apiManagerCategories;
    categoryList = apiManagerCategories.getResponseMutableLiveData();
  }

  public void callCategories() {
    apiManagerCategories.getCallFromApi(null);
  }

  public LiveData<ApiResponse> getCategoryList() {
    return categoryList;
  }

  /**
   * Check if response has an error
   *
   * @return True if it has an error
   */
  public boolean hasCategoryError() {
    ApiResponse value = getCategoryList().getValue();
    return value != null && value.getErrorResponse().getError() != null;
  }
}
