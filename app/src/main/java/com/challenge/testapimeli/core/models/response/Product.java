package com.challenge.testapimeli.core.models.response;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.net.URL;

/** Class to get info from every one of products in the api */
public class Product implements Serializable {
  public static final long serialVersionUID = 43287434328746L;
  @Nullable private String id;
  @Nullable private String title;
  private int price;

  @SerializedName("currency_id")
  @Nullable
  private String currencyId;

  @SerializedName("available_quantity")
  private int availableQuantity;

  @SerializedName("sold_quantity")
  private int soldQuantity;

  @SerializedName("stop_time")
  @Nullable
  private String stopTime;

  @Nullable private URL thumbnail;
  @Nullable private JsonArray pictures;

  @NonNull
  @Override
  public String toString() {
    return "Item{"
        + "id="
        + id
        + ", title='"
        + title
        + '\''
        + ", price="
        + price
        + ", currencyId='"
        + currencyId
        + '\''
        + ", availableQuantity="
        + availableQuantity
        + ", soldQuantity="
        + soldQuantity
        + ", stopTime="
        + stopTime
        + ", thumbnail="
        + thumbnail
        + ", pictures="
        + (pictures != null ? pictures.toString() : "null")
        + '}';
  }

  @Nullable
  public String getId() {
    return id;
  }

  public void setId(@NonNull String id) {
    this.id = id;
  }

  @Nullable
  public String getTitle() {
    return title;
  }

  public void setTitle(@NonNull String title) {
    this.title = title;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  @Nullable
  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(@NonNull String currencyId) {
    this.currencyId = currencyId;
  }

  public int getAvailableQuantity() {
    return availableQuantity;
  }

  public void setAvailableQuantity(int availableQuantity) {
    this.availableQuantity = availableQuantity;
  }

  public int getSoldQuantity() {
    return soldQuantity;
  }

  public void setSoldQuantity(int soldQuantity) {
    this.soldQuantity = soldQuantity;
  }

  @Nullable
  public String getStopTime() {
    return stopTime;
  }

  public void setStopTime(@NonNull String stopTime) {
    this.stopTime = stopTime;
  }

  @Nullable
  public URL getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(@NonNull URL thumbnail) {
    this.thumbnail = thumbnail;
  }

  @Nullable
  public JsonArray getPictures() {
    return pictures;
  }

  public void setPictures(@Nullable JsonArray pictures) {
    this.pictures = pictures;
  }
}
