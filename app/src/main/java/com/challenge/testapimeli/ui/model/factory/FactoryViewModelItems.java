package com.challenge.testapimeli.ui.model.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.challenge.testapimeli.data.api.ApiManagerSearch;
import com.challenge.testapimeli.ui.model.SearchViewModel;
import javax.inject.Inject;

/** Factory to create view model item searching */
@SuppressWarnings("unchecked")
public class FactoryViewModelItems implements ViewModelProvider.Factory {

  ApiManagerSearch apiManagerSearch;

  @Inject
  public FactoryViewModelItems(ApiManagerSearch apiManagerSearch) {
    this.apiManagerSearch = apiManagerSearch;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

    SearchViewModel searchViewModel = new SearchViewModel(apiManagerSearch);
    if (modelClass.isAssignableFrom(searchViewModel.getClass())) {
      return (T) searchViewModel;
    }
    throw new IllegalArgumentException("Unknown ViewModel class");
  }
}
