package com.challenge.testapimeli.core.models.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/** Class to get info paging products from the api */
public class Paging implements Serializable {
  public static final long serialVersionUID = 43287434328747L;

  private int total;

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public int getPrimaryResults() {
    return primaryResults;
  }

  public void setPrimaryResults(int primaryResults) {
    this.primaryResults = primaryResults;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  @SerializedName("primary_results")
  private int primaryResults;

  private int offset;
  private int limit;
}
