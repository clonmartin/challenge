package com.challenge.testapimeli.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.challenge.testapimeli.core.models.response.Category;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.databinding.LayoutItemCategoryBinding;
import javax.inject.Inject;

/** Adapter to show categories in {@link com.challenge.testapimeli.ui.fragment.MainFragment} */
public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.ViewHolder>
    implements View.OnClickListener {
  private ApiResponseCategories categories;
  private View.OnClickListener listener;

  @Inject
  public AdapterCategories() {}

  /**
   * Set categories from the api response
   *
   * @param categories Response from api category
   */
  public void setCategories(@NonNull ApiResponseCategories categories) {
    this.categories = categories;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutItemCategoryBinding view =
        LayoutItemCategoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
    view.getRoot().setOnClickListener(this);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
    holder.category = categories.get(position);
    holder.textView.setText(categories.get(position).getName());
  }

  @Override
  public int getItemCount() {
    return categories.size();
  }

  public void setOnClickListener(@NonNull View.OnClickListener listener) {
    this.listener = listener;
  }

  @Override
  public void onClick(@NonNull View view) {
    if (listener != null) {
      listener.onClick(view);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final TextView textView;
    public final ImageView imageView;
    @Nullable public Category category;

    public ViewHolder(@NonNull LayoutItemCategoryBinding binding) {
      super(binding.getRoot());
      textView = binding.textView;
      imageView = binding.imageView;
    }
  }
}
