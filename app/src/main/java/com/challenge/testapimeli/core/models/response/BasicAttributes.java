package com.challenge.testapimeli.core.models.response;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.Serializable;

/** Class with basic attributes in some objects from the api */
public class BasicAttributes implements Serializable {
  public static final long serialVersionUID = 43287434328744L;
  @Nullable private String id;
  @Nullable private String name;

  @SuppressWarnings("unused")
  @Nullable
  public String getId() {
    return id;
  }

  @SuppressWarnings("unused")
  public void setId(@NonNull String id) {
    this.id = id;
  }

  @Nullable
  public String getName() {
    return name;
  }

  public void setName(@NonNull String name) {
    this.name = name;
  }
}
