package com.challenge.testapimeli.ui.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.challenge.testapimeli.ui.fragment.ImageItemFragment;
import java.util.List;
import javax.inject.Inject;

/** adapter to show images from item api. It Shows all images into the pager view */
public class AdapterImages extends FragmentStateAdapter {
  private List<String> imageList;

  @Inject
  public AdapterImages(@NonNull Fragment fragment) {
    super(fragment);
  }

  public void setImageList(List<String> imageList) {
    this.imageList = imageList;
  }

  /**
   * Show the position of every photo into the pager
   *
   * @param position
   * @return
   */
  @NonNull
  @Override
  public Fragment createFragment(int position) {
    return ImageItemFragment.newInstance(
        imageList.get(position), (position + 1) + "/" + imageList.size());
  }

  @Override
  public int getItemCount() {
    return imageList.size();
  }
}
