package com.challenge.testapimeli.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.R;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.databinding.MainFragmentBinding;
import com.challenge.testapimeli.ui.adapter.AdapterCategories;
import com.challenge.testapimeli.ui.main.MainActivity;
import com.challenge.testapimeli.ui.model.MainViewModel;
import com.challenge.testapimeli.ui.model.factory.FactoryViewModelCategory;
import com.google.android.material.snackbar.Snackbar;
import dagger.hilt.android.AndroidEntryPoint;
import javax.inject.Inject;

/** Initial fragment. It shows the searching edit text and a list of categories */
@AndroidEntryPoint
public class MainFragment extends FragmentBase {

  private static final String TAG = "TAG ".concat(MainFragment.class.getName());
  public static final String QUERY_SEARCH = "query";
  @Nullable private MainViewModel mViewModel;
  @Nullable private MainFragmentBinding binding;

  @Inject FactoryViewModelCategory factoryViewModelCategory;
  @Inject AdapterCategories adapter;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = MainFragmentBinding.inflate(inflater, container, false);

    binding.buttonSearch.setOnClickListener(
        l -> {
          if (binding.editTextSearchProduct.getText() != null) {
            Bundle bundle = new Bundle();
            bundle.putString(QUERY_SEARCH, binding.editTextSearchProduct.getText().toString());
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_mainFragment_to_itemsFragment, bundle);
          }
        });

    if (mViewModel == null || mViewModel.hasCategoryError()) {
      return binding.getRoot();
    }

    if (mViewModel.getCategoryList().getValue() != null) {
      Log.d(TAG, "onCreateView: getValue");
      getResults(mViewModel.getCategoryList().getValue());
    } else {
      mViewModel.callCategories();
    }

    return binding.getRoot();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mViewModel = new ViewModelProvider(this, factoryViewModelCategory).get(MainViewModel.class);
    // Use the ViewModel

    mViewModel.getCategoryList().observe(this, this::getResults);
  }

  /**
   * To manage categories *
   *
   * @param apiResponse List of categories
   */
  @Override
  protected void getResults(ApiResponse apiResponse) {
    if (BuildConfig.DEBUG) Log.d(TAG, "getResults: apiResponse" + apiResponse);
    if (this.checkErrors(apiResponse)) {
      Log.e(TAG, "getResults: error ");
      return;
    }

    ApiResponseCategories categories = (ApiResponseCategories) apiResponse;
    fillListViewCategories(categories);
  }

  /**
   * Get response and fillth Recycler view
   *
   * @param categories Api response bring by the observer
   */
  private void fillListViewCategories(ApiResponseCategories categories) {
    if (categories.size() > 0) {
      if (binding != null) {
        LinearLayoutManager horizontalLayoutManager =
            new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false);
        binding.pagerCategories.setLayoutManager(horizontalLayoutManager);
        adapter.setCategories(categories);
        adapter.setOnClickListener(
            view -> {
              String name =
                  categories.get(binding.pagerCategories.getChildAdapterPosition(view)).getName();
              Bundle bundle = new Bundle();
              bundle.putString(QUERY_SEARCH, name);
              NavHostFragment.findNavController(MainFragment.this)
                  .navigate(R.id.action_mainFragment_to_itemsFragment, bundle);
            });
        binding.pagerCategories.setAdapter(adapter);
      }
    } else {
      // Zero categories
      Log.d(TAG, "getResults: ZERO_RESULTS");
      if (binding != null) {
        Snackbar.make(
                requireContext(),
                binding.getRoot(),
                getString(R.string.zero_results),
                Snackbar.LENGTH_LONG)
            //                    .setAction("volver",action->
            // NavHostFragment.findNavController(MainFragment.this).popBackStack())
            .show();
      }
    }
    idleTest();
  }

  @VisibleForTesting
  private void idleTest() {
    MainActivity mainActivity = (MainActivity) requireActivity();
    mainActivity.setmIdlingResourceState(true);
  }
}
