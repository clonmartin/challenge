package com.challenge.testapimeli.data.response;

import androidx.annotation.Nullable;
import com.challenge.testapimeli.core.models.error.ErrorStandard;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import okhttp3.ResponseBody;

/** Manage errors in retrofit callbacks */
public class ErrorResponse {

  /**
   * This error is from fault callback. But you can use to set errors using {@link Exception}
   * object.
   */
  @Nullable private Throwable error;

  /**
   * Standard error from api. See <a
   * href="https://developers.mercadolibre.com.ar/es_ar/consideraciones-de-diseno#Manejo-de-errores">standard
   * errors</a>
   */
  @Nullable private ErrorStandard errorStandard;

  @Nullable
  public Throwable getError() {
    return error;
  }

  public void setError(@Nullable Throwable error) {
    this.error = error;
  }

  @Nullable
  public ErrorStandard getErrorStandard() {
    return errorStandard;
  }

  public void setErrorStandard(@Nullable ErrorStandard errorStandard) {
    this.errorStandard = errorStandard;
  }

  /**
   * Convert callback response error to {@link ErrorStandard}
   *
   * @param responseBody Callback response error
   * @return Standard error from api
   * @throws IOException In out exception
   * @throws JsonSyntaxException Parser exception
   */
  @Nullable
  public static ErrorStandard jsonToErrorResponse(@Nullable ResponseBody responseBody)
      throws IOException, JsonIOException, JsonSyntaxException {
    if (responseBody != null) {
      JsonObject jsonObjError = new JsonObject();
      jsonObjError.addProperty("error", responseBody.string());
      Gson gson = new GsonBuilder().create();
      return gson.fromJson(
          String.valueOf(jsonObjError.get("error").getAsString()), ErrorStandard.class);
    } else {
      return null;
    }
  }
}
