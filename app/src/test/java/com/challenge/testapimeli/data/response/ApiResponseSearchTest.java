package com.challenge.testapimeli.data.response;

import static org.junit.Assert.*;

import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.core.models.response.Paging;
import com.challenge.testapimeli.core.models.response.Product;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.RetrofitMockService;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class ApiResponseSearchTest {

  private RetrofitMockService.MockApiService mockApiService;
  private ApiResponseSearch body;

  @Before
  public void setUp() throws Exception {

    // Create a very simple Retrofit adapter which points the GitHub API.
    Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.SERVER_PATH).build();

    // Create a MockRetrofit object with a NetworkBehavior which manages the fake behavior of calls.
    NetworkBehavior behavior = NetworkBehavior.create();
    MockRetrofit mockRetrofit =
        new MockRetrofit.Builder(retrofit).networkBehavior(behavior).build();

    BehaviorDelegate<ApiService> delegate = mockRetrofit.create(ApiService.class);
    mockApiService = new RetrofitMockService.MockApiService(delegate, false);
    mockApiService.addSearch(StringTest.JSON_SEARCH);
    Call<ApiResponseSearch> apiResponseSearchCall = mockApiService.callSearchProduct("", 0, 0);
    body = apiResponseSearchCall.execute().body();
  }

  @Test
  public void getErrorResponse() {
    assertNotNull(new ApiResponseSearch().getErrorResponse());
  }

  @Test
  public void checkSearchingItems() {

    List<Product> products = body.getResults();
    assertTrue(products.size() > 0);
    for (Product product : products) {
      System.out.println(product.getId());
      assertNotNull(product.getId());
      assertNotNull(product.getCurrencyId());
      assertTrue(product.getPrice() > 0);
      assertNotNull(product.getThumbnail());
      assertNotNull(product.getTitle());
    }
  }

  @Test
  public void checkSearchingPaging() {
    Paging paging = body.getPaging();
    assertNotNull(paging);
    assertSame(50, paging.getOffset());
    assertSame(2, paging.getLimit());
    assertEquals(1837278, paging.getTotal());
  }
}
