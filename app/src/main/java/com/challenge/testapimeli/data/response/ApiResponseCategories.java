package com.challenge.testapimeli.data.response;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.core.models.response.Category;
import java.util.ArrayList;

/** Class for response in callback categories. */
public class ApiResponseCategories extends ArrayList<Category> implements ApiResponse {
  public static final long serialVersionUID = 43287434328747L;

  private ErrorResponse errorResponse = new ErrorResponse();

  @NonNull
  @Override
  public ErrorResponse getErrorResponse() {
    return errorResponse;
  }

  // If the response is an error then set the error
  @Override
  public void setErrorResponse(ErrorResponse errorResponse) {
    this.errorResponse = errorResponse;
  }
}
