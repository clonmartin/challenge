package com.challenge.testapimeli.ui.model.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.challenge.testapimeli.data.api.ApiManagerDescription;
import com.challenge.testapimeli.data.api.ApiManagerItems;
import com.challenge.testapimeli.ui.model.ItemViewModel;
import javax.inject.Inject;

/** Factory to create view model item product */
@SuppressWarnings("unchecked")
public class FactoryViewModelItem implements ViewModelProvider.Factory {
  ApiManagerItems apiManagerItems;
  ApiManagerDescription apiManagerDescription;

  @Inject
  public FactoryViewModelItem(
      ApiManagerItems apiManagerItems, ApiManagerDescription apiManagerDescription) {
    this.apiManagerItems = apiManagerItems;
    this.apiManagerDescription = apiManagerDescription;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

    ItemViewModel itemViewModel = new ItemViewModel(apiManagerItems, apiManagerDescription);
    if (modelClass.isAssignableFrom(itemViewModel.getClass())) {
      return (T) itemViewModel;
    }
    throw new IllegalArgumentException("Unknown ViewModel class");
  }
}
