package com.challenge.testapimeli.data.response;

import static org.junit.Assert.*;

import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.core.models.response.Item;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.RetrofitMockService;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class ApiResponseItemTest {

  private RetrofitMockService.MockApiService mockApiService;

  @Before
  public void setUp() {

    // Create a very simple Retrofit adapter which points the GitHub API.
    Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.SERVER_PATH).build();

    // Create a MockRetrofit object with a NetworkBehavior which manages the fake behavior of calls.
    NetworkBehavior behavior = NetworkBehavior.create();
    MockRetrofit mockRetrofit =
        new MockRetrofit.Builder(retrofit).networkBehavior(behavior).build();

    BehaviorDelegate<ApiService> delegate = mockRetrofit.create(ApiService.class);
    mockApiService = new RetrofitMockService.MockApiService(delegate, false);
    mockApiService.addItem(StringTest.JSON_ITEMS);
  }

  @Test
  public void getErrorResponse() {
    assertNotNull(new ApiResponseItem().getErrorResponse());
  }

  @Test
  public void checkIdItems() throws IOException {
    Call<ApiResponseItem> apiResponseItemCall = mockApiService.callItemAttributes("", "");
    ApiResponseItem body = apiResponseItemCall.execute().body();
    for (Item item : body) {
      assertTrue(item.getProduct().getId().length() > 0);
    }
  }

  @Test
  public void checkJsonArrayItems() throws IOException {
    Call<ApiResponseItem> apiResponseItemCall = mockApiService.callItemAttributes("", "");
    ApiResponseItem body = apiResponseItemCall.execute().body();
    for (Item item : body) {
      assertTrue(item.getProduct().getPictures().isJsonArray());
      assertTrue(item.getProduct().getPictures().size() > 0);
    }
  }
}
