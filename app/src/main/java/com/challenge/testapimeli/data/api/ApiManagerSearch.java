package com.challenge.testapimeli.data.api;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.api.callback.CallbackSearch;
import com.challenge.testapimeli.data.request.ApiRequest;
import com.challenge.testapimeli.data.request.ApiRequestSearch;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;

/** Manage data api item search */
public class ApiManagerSearch implements ApiManager {
  private final MutableLiveData<ApiResponse> responseMutableLiveData;
  private final ApiService apiService;
  private final Callback<ApiResponseSearch> callback;

  /**
   * It sends a mutable live data to set into callback when the api responses
   *
   * @param apiService Api service interface
   */
  @Inject
  public ApiManagerSearch(ApiService apiService) {
    this.responseMutableLiveData = new MutableLiveData<>();
    this.apiService = apiService;
    this.callback = new CallbackSearch(responseMutableLiveData);
  }

  /**
   * Mutable live data
   *
   * @return A mutable live data set from callback response
   */
  @NonNull
  public MutableLiveData<ApiResponse> getResponseMutableLiveData() {
    return responseMutableLiveData;
  }

  /** Create call to get products */
  @Override
  public void getCallFromApi(@Nullable ApiRequest apiRequest) {
    if (apiRequest != null) {
      ApiRequestSearch requestSearch = (ApiRequestSearch) apiRequest;
      String query = (requestSearch.getQuery() != null) ? requestSearch.getQuery() : "";
      Call<ApiResponseSearch> call =
          apiService.callSearchProduct(query, requestSearch.getLimit(), requestSearch.getOffset());
      call.enqueue(callback);
    } else {
      Log.d("TAG", "getCallFromApi: ");
    }
  }
}
