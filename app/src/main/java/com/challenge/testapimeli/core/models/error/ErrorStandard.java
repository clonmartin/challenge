package com.challenge.testapimeli.core.models.error;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.JsonArray;
import java.io.Serializable;

/**
 * Class to model standard error from api. See <a
 * href="https://developers.mercadolibre.com.ar/es_ar/consideraciones-de-diseno#Manejo-de-errores">
 * standard errors</a>
 */
@SuppressWarnings("ALL")
public class ErrorStandard implements Serializable {
  public static final long serialVersionUID = 43287434328743L;
  @Nullable private String message;
  @Nullable private String error;
  private int status;
  @Nullable private JsonArray cause;

  public String getMessage() {
    return message;
  }

  public void setMessage(@NonNull String message) {
    this.message = message;
  }

  @Nullable
  public String getError() {
    return error;
  }

  public void setError(@NonNull String error) {
    this.error = error;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  @Nullable
  public JsonArray getCause() {
    return cause;
  }

  public void setCause(@NonNull JsonArray cause) {
    this.cause = cause;
  }
}
