package com.challenge.testapimeli;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.IdlingResource;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.MediumTest;
import com.challenge.testapimeli.ui.main.MainActivity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class FlowCategoryTest {
  private IdlingResource mIdlingResource;

  @Rule
  public ActivityScenarioRule<MainActivity> activityScenarioRule =
      new ActivityScenarioRule<>(MainActivity.class);

  @Before
  public void registerIdlingResource() {
    ActivityScenario activityScenario = ActivityScenario.launch(MainActivity.class);
    activityScenario.onActivity(
        (ActivityScenario.ActivityAction<MainActivity>)
            activity -> {
              mIdlingResource = activity.getIdlingResource();
              // To prove that the test fails, omit this call:
              IdlingRegistry.getInstance().register(mIdlingResource);
            });
  }

  @Test
  public void clickCategoriesToListProductAndClickProduct() {
    onView(withId(R.id.pager_categories))
        .perform(RecyclerViewActions.actionOnItemAtPosition(6, click()));
    onView(withId(R.id.main)).check(ViewAssertions.doesNotExist());
    onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(6, click()));
    onView(withId(R.id.list)).check(ViewAssertions.doesNotExist());
  }
}
