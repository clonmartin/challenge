package com.challenge.testapimeli.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.challenge.testapimeli.R;
import com.challenge.testapimeli.core.models.response.Product;
import com.challenge.testapimeli.data.request.ApiRequestSearch;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import com.challenge.testapimeli.databinding.FragmentSearchBinding;
import com.challenge.testapimeli.ui.adapter.AdapterItems;
import com.challenge.testapimeli.ui.main.MainActivity;
import com.challenge.testapimeli.ui.model.SearchViewModel;
import com.challenge.testapimeli.ui.model.factory.FactoryViewModelItems;
import com.google.android.material.snackbar.Snackbar;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.List;
import javax.inject.Inject;

/** A fragment representing a list of Items. */
@AndroidEntryPoint
public class SearchFragment extends FragmentBase {

  private static final String TAG = "TAG ".concat(SearchViewModel.class.getSimpleName());
  public static final String ID_PRODUCT = "id_product";
  private int mColumnCount = 1;
  private SearchViewModel mViewModel;
  private FragmentSearchBinding binding;

  @Inject FactoryViewModelItems factoryViewModelItems;
  @Inject ApiRequestSearch requestSearch;
  @Inject AdapterItems adapter;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon
   * screen orientation changes).
   */
  public SearchFragment() {}

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mViewModel = new ViewModelProvider(this, factoryViewModelItems).get(SearchViewModel.class);
    mViewModel.getSearchResult().observe(this, this::getResults);
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    binding = FragmentSearchBinding.inflate(inflater, container, false);
    RecyclerView view = binding.getRoot();

    // Set adapter and onClick in every one of items
    view.setLayoutManager(new GridLayoutManager(requireContext(), mColumnCount));
    view.setAdapter(adapter);
    adapter.setOnClickListener(
        v -> {
          mViewModel.setShowingItem(true);
          String idProduct = adapter.getProductList().get(view.getChildAdapterPosition(v)).getId();
          Bundle bundle = new Bundle();
          bundle.putString(ID_PRODUCT, idProduct);
          NavHostFragment.findNavController(SearchFragment.this)
              .navigate(R.id.action_itemFragment_to_infoFragment, bundle);
        });

    view.addOnScrollListener(
        new RecyclerView.OnScrollListener() {
          @Override
          public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (!recyclerView.canScrollVertically(1)
                && newState == RecyclerView.SCROLL_STATE_IDLE) {
              Log.d("-----", "end");
              mViewModel.updateNewItems(requestSearch.getQuery());
              Toast.makeText(requireContext(), "Reload", Toast.LENGTH_SHORT).show();
            }
          }
        });

    if (getArguments() != null && !mViewModel.hasSearchError()) {

      String query = getArguments().getString(MainFragment.QUERY_SEARCH);
      requestSearch.setQuery(query);

      Log.d(TAG, "onCreateView: ".concat(query));
      mViewModel.callSearch(requestSearch);

    } else {
      NavHostFragment.findNavController(this).popBackStack();
    }
    return view;
  }

  protected void getResults(ApiResponse apiResponse) {
    if (apiResponse != null) {
      if (checkErrors(apiResponse)) {
        return;
      }
      ApiResponseSearch responseSearch = (ApiResponseSearch) apiResponse;
      List<Product> result = responseSearch.getResults();

      if (result.isEmpty() && adapter.getItemCount() == 0) {
        Snackbar.make(
                requireContext(),
                binding.getRoot(),
                getString(R.string.zero_results),
                Snackbar.LENGTH_INDEFINITE)
            .setAction(
                R.string.back, action -> NavHostFragment.findNavController(this).popBackStack())
            .show();
      }

      Log.d(TAG, "getResults: " + mViewModel.isshowingItem());
      if (!mViewModel.isshowingItem()) {
        adapter.addProductListItems(result);
        binding
            .getRoot()
            .smoothScrollToPosition(mViewModel.showScrollItemToStart(adapter.getItemCount()));
      } else {
        mViewModel.setShowingItem(false);
      }
      Log.d(TAG, "getResults: " + mViewModel.isshowingItem());
      idleTest();
    }
  }

  @VisibleForTesting
  private void idleTest() {
    MainActivity mainActivity = (MainActivity) requireActivity();
    mainActivity.setmIdlingResourceState(true);
  }
}
