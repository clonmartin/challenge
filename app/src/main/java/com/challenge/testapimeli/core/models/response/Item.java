package com.challenge.testapimeli.core.models.response;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/** Class to get product info from the api */
public class Item {
  protected int code;
  @Nullable protected Product body;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  @Nullable
  public Product getProduct() {
    return body;
  }

  public void setProducts(@NonNull Product product) {
    this.body = product;
  }
}
