package com.challenge.testapimeli.data.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.api.callback.CallbackCategories;
import com.challenge.testapimeli.data.request.ApiRequest;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;

/** Manage data api categories */
public class ApiManagerCategories implements ApiManager {
  private final MutableLiveData<ApiResponse> responseMutableLiveData;
  private final Callback<ApiResponseCategories> arrayListCallback;
  private final ApiService apiService;

  /**
   * It sends a mutable live data to set into callback when the api responses
   *
   * @param apiService Api service interface
   */
  @Inject
  public ApiManagerCategories(@NonNull ApiService apiService) {
    this.apiService = apiService;
    responseMutableLiveData = new MutableLiveData<>();
    arrayListCallback = new CallbackCategories(responseMutableLiveData);
  }

  /**
   * Mutable live data
   *
   * @return A mutable live data set from callback response
   */
  @NonNull
  public MutableLiveData<ApiResponse> getResponseMutableLiveData() {
    return responseMutableLiveData;
  }

  /** Create call to get categories */
  @Override
  public void getCallFromApi(@Nullable ApiRequest apiRequest) {
    Call<ApiResponseCategories> arrayListCall = apiService.callCategories();
    arrayListCall.enqueue(arrayListCallback);
  }
}
