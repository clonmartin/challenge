package com.challenge.testapimeli.data.di;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.data.ApiService;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.Contract;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/** Module for retrofit with dagger hilt */
@Module
@InstallIn(SingletonComponent.class)
public class NetworkModule {

  /**
   * Retrofit
   *
   * @param okHttpClient See {@link OkHttpClient}
   * @return Retrofit
   */
  @NonNull
  @Contract("_ -> new")
  @Singleton
  @Provides
  public static Retrofit ApiAdapter(OkHttpClient okHttpClient) {
    return new Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_PATH)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient) // <-- use log level
        .build();
  }

  /**
   * Http interceptor
   *
   * @param logging See {@link HttpLoggingInterceptor}
   * @return OkHttpClient
   */
  @NonNull
  @Singleton
  @Provides
  public static OkHttpClient getHttpClient(@NonNull HttpLoggingInterceptor logging) {
    // Interceptor to get body log
    logging.setLevel(HttpLoggingInterceptor.Level.BODY);

    // Add interceptor
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.readTimeout(30, TimeUnit.SECONDS);
    httpClient.connectTimeout(30, TimeUnit.SECONDS);
    httpClient.addInterceptor(logging);
    return httpClient.build();
  }

  /**
   * Retrofit adapter
   *
   * @return Retrofit adapter
   */
  @NonNull
  @Singleton
  @Provides
  public static ApiService getApiService() {
    return ApiAdapter(getHttpClient(new HttpLoggingInterceptor())).create(ApiService.class);
  }
}
