package com.challenge.testapimeli.core;

import android.app.Application;
import dagger.hilt.android.HiltAndroidApp;

/** Class to initialize dagger hilt */
@HiltAndroidApp
public class ApplicationMeLi extends Application {}
