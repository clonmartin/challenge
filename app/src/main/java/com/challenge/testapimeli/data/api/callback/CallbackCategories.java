package com.challenge.testapimeli.data.api.callback;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.api.ApiManagerCategories;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/** Callback to get list of categories */
public class CallbackCategories implements Callback<ApiResponseCategories>, CallbackHelperError {

  private static final String TAG = "TAG ".concat(CallbackCategories.class.getSimpleName());

  // Mutable live data to storage and set api response
  // If response fails then we use fail response to set the error response into livedata
  private final MutableLiveData<ApiResponse> liveData;

  /**
   * Constructor to assign live data from api manager {@link ApiManagerCategories}
   *
   * @param responseMutableLiveData Live data from api manager.
   */
  public CallbackCategories(MutableLiveData<ApiResponse> responseMutableLiveData) {
    liveData = responseMutableLiveData;
  }

  @Override
  public void onResponse(
      @NonNull Call<ApiResponseCategories> call,
      @NonNull Response<ApiResponseCategories> response) {

    if (response.isSuccessful() && response.body() != null) {
      Log.d(TAG, "onResponse: is success. Items:".concat(String.valueOf((response.body()).size())));
      ApiResponseCategories apiResponseCategories = response.body();
      apiResponseCategories.getErrorResponse().setError(null);
      Log.d(TAG, "onResponse: liveData.setValue ok");
      liveData.setValue(apiResponseCategories);

    } else {
      Log.d(TAG, "onResponse: fail");
      if (response.errorBody() != null) {
        ApiResponse bodyError = getBodyError(response.errorBody());
        Log.d(TAG, "onResponse: liveData.setValue fail");
        liveData.setValue(bodyError);
        return;
      }

      ApiResponse failResponse = new ApiResponseCategories();
      failResponse.getErrorResponse().setError(new Exception());
      Log.d(TAG, "onResponse: liveData.setValue fail");
      liveData.setValue(failResponse);
    }
  }

  @Override
  public void onFailure(@NonNull Call<ApiResponseCategories> call, @NonNull Throwable t) {
    ApiResponse failResponse = new ApiResponseCategories();
    failResponse.getErrorResponse().setError(t);
    Log.e(TAG, "onFailure: ".concat(t.toString()), t);
    liveData.setValue(failResponse);
  }
}
