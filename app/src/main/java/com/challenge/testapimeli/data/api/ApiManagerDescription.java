package com.challenge.testapimeli.data.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.api.callback.CallbackDescription;
import com.challenge.testapimeli.data.request.ApiRequest;
import com.challenge.testapimeli.data.request.ApiRequestDescription;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseDescription;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;

/** Manage data api description */
public class ApiManagerDescription implements ApiManager {
  private final MutableLiveData<ApiResponse> responseMutableLiveData;
  private final ApiService apiService;
  private final Callback<ApiResponseDescription> callback;

  /**
   * It sends a mutable live data to set into callback when the api responses
   *
   * @param apiService Api service interface
   */
  @Inject
  public ApiManagerDescription(@NonNull ApiService apiService) {
    this.responseMutableLiveData = new MutableLiveData<>();
    this.apiService = apiService;
    this.callback = new CallbackDescription(responseMutableLiveData);
  }

  /**
   * Mutable live data
   *
   * @return A mutable live data set from callback response
   */
  @NonNull
  @Override
  public MutableLiveData<ApiResponse> getResponseMutableLiveData() {
    return responseMutableLiveData;
  }

  /** Create call to get description item */
  @Override
  public void getCallFromApi(@Nullable ApiRequest apiRequest) {
    if (apiRequest != null) {
      ApiRequestDescription requestDescription = (ApiRequestDescription) apiRequest;
      Call<ApiResponseDescription> descriptionCall =
          apiService.callDescription(requestDescription.getIdProduct());

      descriptionCall.enqueue(callback);
    }
  }
}
