package com.challenge.testapimeli.ui.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;
import androidx.test.espresso.IdlingResource;
import com.challenge.testapimeli.databinding.MainActivityBinding;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.concurrent.atomic.AtomicBoolean;

/** This activity contains all fragments. It works with nav_host_fragment and nav_graph */
@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

  MainActivityBinding binding;

  // The Idling Resource which will be null in production.
  @Nullable private SimpleIdlingResource mIdlingResource;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = MainActivityBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());
  }

  @VisibleForTesting
  public void setmIdlingResourceState(Boolean state) {
    if (mIdlingResource != null) {
      mIdlingResource.setIdleState(state);
    }
  }
  /** Only called from test, creates and returns a new {@link SimpleIdlingResource}. */
  @VisibleForTesting
  @NonNull
  public IdlingResource getIdlingResource() {
    if (mIdlingResource == null) {
      mIdlingResource = new SimpleIdlingResource();
    }
    mIdlingResource.setIdleState(false);
    return mIdlingResource;
  }

  public class SimpleIdlingResource implements IdlingResource {

    @Nullable private volatile ResourceCallback mCallback;

    // Idleness is controlled with this boolean.
    private AtomicBoolean mIsIdleNow = new AtomicBoolean(true);

    @Override
    public String getName() {
      return this.getClass().getName();
    }

    @Override
    public boolean isIdleNow() {
      return mIsIdleNow.get();
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
      mCallback = callback;
    }

    /**
     * Sets the new idle state, if isIdleNow is true, it pings the {@link ResourceCallback}.
     *
     * @param isIdleNow false if there are pending operations, true if idle.
     */
    public void setIdleState(boolean isIdleNow) {
      mIsIdleNow.set(isIdleNow);
      if (isIdleNow && mCallback != null) {
        mCallback.onTransitionToIdle();
      }
    }
  }
}
