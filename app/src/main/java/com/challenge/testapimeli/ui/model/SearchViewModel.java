package com.challenge.testapimeli.ui.model;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.challenge.testapimeli.core.models.response.Paging;
import com.challenge.testapimeli.data.api.ApiManagerSearch;
import com.challenge.testapimeli.data.request.ApiRequestSearch;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

/**
 * This view model uses api manager to send request and get responses setting searchResult from
 * callback
 */
@HiltViewModel
public class SearchViewModel extends ViewModel {
  @NonNull private final LiveData<ApiResponse> searchResult;
  @NonNull private final ApiManagerSearch apiManagerSearch;
  private int newOffset = 0;

  public boolean isshowingItem() {
    return showingItem;
  }

  public void setShowingItem(boolean showingItem) {
    this.showingItem = showingItem;
  }

  private boolean showingItem;

  @Inject
  public SearchViewModel(@NonNull ApiManagerSearch apiManagerSearch) {
    this.apiManagerSearch = apiManagerSearch;
    this.searchResult = apiManagerSearch.getResponseMutableLiveData();
  }

  public void callSearch(@NonNull ApiRequestSearch search) {
    this.apiManagerSearch.getCallFromApi(search);
  }

  @NonNull
  public LiveData<ApiResponse> getSearchResult() {
    return searchResult;
  }

  /**
   * Set Query to call new items when the list is over. It works until total is less than offset
   *
   * @param query Product to search.
   */
  public void updateNewItems(String query) {
    ApiResponseSearch lastSearch = (ApiResponseSearch) getSearchResult().getValue();
    if (lastSearch != null) {
      Paging paging = lastSearch.getPaging();
      newOffset += paging.getLimit();
      if (newOffset > paging.getTotal()) return;
      ApiRequestSearch requestSearch = new ApiRequestSearch(null, paging.getLimit(), newOffset);

      requestSearch.setQuery(query);

      Log.d("TAG", "onCreateView: ".concat(query));
      callSearch(requestSearch);
    }
  }

  /**
   * When the list is over and updateNewItems calls new products then it scrolls to the near
   * position.
   *
   * @param totalListSize Size of list before calls updateNewItems
   * @return int position to scroll
   */
  public int showScrollItemToStart(int totalListSize) {
    ApiResponseSearch lastSearch = (ApiResponseSearch) getSearchResult().getValue();
    int result = 0;
    if (lastSearch != null) {
      result = lastSearch.getResults().size();
    }
    int startItemToSee = 2;
    return Math.max(totalListSize - result + startItemToSee, 0);
  }

  /**
   * Check if response has an error
   *
   * @return True if it has an error
   */
  public boolean hasSearchError() {
    ApiResponse value = getSearchResult().getValue();
    return value != null && value.getErrorResponse().getError() != null;
  }
}
