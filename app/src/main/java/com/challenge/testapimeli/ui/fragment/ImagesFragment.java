package com.challenge.testapimeli.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.challenge.testapimeli.databinding.FragmentImagesBinding;
import com.challenge.testapimeli.ui.adapter.AdapterImages;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.List;
import javax.inject.Inject;

/** Fragment to show the view pager */
@AndroidEntryPoint
public class ImagesFragment extends Fragment {
  private List<String> imageList;
  FragmentImagesBinding binding;

  @Inject AdapterImages adapterImages;

  public ImagesFragment() {
    // Required empty public constructor

  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      imageList = getArguments().getStringArrayList(ItemFragment.IMAGES_LIST);
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
    binding = FragmentImagesBinding.inflate(inflater, container, false);

    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    adapterImages.setImageList(imageList);
    binding.pager.setAdapter(adapterImages);
  }
}
