package com.challenge.testapimeli.data.response;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.core.models.response.BasicAttributes;
import com.challenge.testapimeli.core.models.response.Paging;
import com.challenge.testapimeli.core.models.response.Product;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.Nullable;

/** Response of searching call */
public class ApiResponseSearch implements Serializable, ApiResponse {
  public static final long serialVersionUID = 43287434328748L;

  @SerializedName("site_id")
  @NonNull
  private String siteId;

  @NonNull private Paging paging;
  @NonNull private List<Product> results;
  @Nullable private BasicAttributes sort;

  @SerializedName("available_sorts")
  @Nullable
  private List<BasicAttributes> availableSorts;

  public ApiResponseSearch() {
    this.siteId = "";
    this.paging = new Paging();
    this.results = new ArrayList<>();
  }

  @NonNull
  public String getSiteId() {
    return siteId;
  }

  public void setSiteId(@NonNull String siteId) {
    this.siteId = siteId;
  }

  @NonNull
  public Paging getPaging() {
    return paging;
  }

  public void setPaging(@NonNull Paging paging) {
    this.paging = paging;
  }

  @NonNull
  public List<Product> getResults() {
    return results;
  }

  public void setResults(@NonNull List<Product> results) {
    this.results = results;
  }

  @Nullable
  public BasicAttributes getSort() {
    return sort;
  }

  public void setSort(@Nullable BasicAttributes sort) {
    this.sort = sort;
  }

  @Nullable
  public List<BasicAttributes> getAvailableSorts() {
    return availableSorts;
  }

  public void setAvailableSorts(@Nullable List<BasicAttributes> availableSorts) {
    this.availableSorts = availableSorts;
  }

  private ErrorResponse errorResponse = new ErrorResponse();

  @NonNull
  @Override
  public ErrorResponse getErrorResponse() {
    return errorResponse;
  }

  // If the response is an error then set the error
  @Override
  public void setErrorResponse(ErrorResponse errorResponse) {
    this.errorResponse = errorResponse;
  }
}
