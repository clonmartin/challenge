package com.challenge.testapimeli.data.api.callback;

import android.util.Log;
import com.challenge.testapimeli.core.models.error.ErrorStandard;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.data.response.ErrorResponse;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import okhttp3.ResponseBody;

/** Callback interface to manage json error */
public interface CallbackHelperError {

  default ApiResponse getBodyError(ResponseBody responseBody) {
    ApiResponseCategories failResponse = new ApiResponseCategories();
    try {
      ErrorStandard error = ErrorResponse.jsonToErrorResponse(responseBody);
      if (error != null) {
        failResponse.getErrorResponse().setError(new Exception(error.getMessage()));
        failResponse.getErrorResponse().setErrorStandard(error);
      } else {
        failResponse.getErrorResponse().setError(new NullPointerException());
      }
    } catch (JsonIOException | IOException | JsonSyntaxException e) {
      String TAG = "TAG CallbackHelperError";
      Log.e(TAG, "onResponse: fail when error is setting", e);
      failResponse.getErrorResponse().setError(e);
    }
    return failResponse;
  }
}
