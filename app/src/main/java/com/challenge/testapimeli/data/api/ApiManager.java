package com.challenge.testapimeli.data.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.request.ApiRequest;
import com.challenge.testapimeli.data.response.ApiResponse;

/** Interface to create an api manager from every endpoint */
public interface ApiManager {

  /** Mutable live data to storage and observe every call to api */
  @NonNull
  MutableLiveData<ApiResponse> getResponseMutableLiveData();

  /**
   * Use to call an api endpoint
   *
   * @param apiRequest Set params to call an api endpoint
   */
  void getCallFromApi(@Nullable ApiRequest apiRequest);
}
