package com.challenge.testapimeli.data.response;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.core.models.response.Item;
import java.io.Serializable;
import java.util.ArrayList;

/** Class for response in callback item */
public class ApiResponseItem extends ArrayList<Item> implements ApiResponse, Serializable {
  public static final long serialVersionUID = 43287434328749L;

  private ErrorResponse errorResponse = new ErrorResponse();

  @NonNull
  @Override
  public ErrorResponse getErrorResponse() {
    return errorResponse;
  }

  // If the response is an error then set the error
  @Override
  public void setErrorResponse(ErrorResponse errorResponse) {
    this.errorResponse = errorResponse;
  }
}
