package com.challenge.testapimeli.data.api.callback;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/** Callback to get a list of searching items */
public class CallbackSearch implements Callback<ApiResponseSearch>, CallbackHelperError {

  private static final String TAG = "TAG ".concat(CallbackSearch.class.getName());

  // Mutable live data to storage and set api response
  // If response fails then we use fail response to set the error response into livedata
  private final MutableLiveData<ApiResponse> liveData;

  public CallbackSearch(MutableLiveData<ApiResponse> responseMutableLiveData) {
    this.liveData = responseMutableLiveData;
  }

  @Override
  public void onResponse(
      @NonNull Call<ApiResponseSearch> call, @NonNull Response<ApiResponseSearch> response) {
    if (response.isSuccessful() && response.body() != null) {
      Log.d(
          TAG,
          "onResponse: is success. Items:"
              .concat(String.valueOf((response.body()).getResults().size())));
      ApiResponseSearch apiResponseSearch = response.body();
      apiResponseSearch.getErrorResponse().setError(null);
      liveData.setValue(apiResponseSearch);
    } else {
      Log.d(TAG, "onResponse: fail");
      if (response.errorBody() != null) {
        ApiResponse bodyError = getBodyError(response.errorBody());
        liveData.setValue(bodyError);
        return;
      }
      ApiResponse failResponse = new ApiResponseCategories();
      failResponse.getErrorResponse().setError(new Exception());
      liveData.setValue(failResponse);
    }
  }

  @Override
  public void onFailure(@NonNull Call<ApiResponseSearch> call, @NonNull Throwable t) {
    ApiResponse failResponse = new ApiResponseCategories();
    failResponse.getErrorResponse().setError(t);
    Log.e(TAG, "onFailure: ".concat(t.toString()), t);
    liveData.setValue(failResponse);
  }
}
