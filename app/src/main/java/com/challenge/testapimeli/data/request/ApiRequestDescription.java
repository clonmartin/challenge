package com.challenge.testapimeli.data.request;

import android.util.Log;
import com.challenge.testapimeli.BuildConfig;

/** Implementation class to create parameters to get a description item from the api */
public class ApiRequestDescription implements ApiRequest {
  private static final String TAG = "TAG ".concat(ApiRequestDescription.class.getSimpleName());
  private String idProduct;

  /**
   * Id item product to get description
   *
   * @param idProduct Id item product
   */
  public ApiRequestDescription(String idProduct) {
    if (BuildConfig.DEBUG) Log.d(TAG, "ApiRequestDescription: " + idProduct);
    this.idProduct = idProduct;
  }

  public String getIdProduct() {
    if (BuildConfig.DEBUG) Log.d(TAG, "ApiRequestDescription: " + idProduct);
    return idProduct;
  }

  public void setIdProduct(String idProduct) {
    this.idProduct = idProduct;
  }
}
