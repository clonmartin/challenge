package com.challenge.testapimeli.ui.fragment;

import static com.challenge.testapimeli.ui.fragment.ErrorFragment.ARG_PARAM1;
import static com.challenge.testapimeli.ui.fragment.ErrorFragment.ARG_PARAM2;

import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import com.challenge.testapimeli.R;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ErrorResponse;

/** Base to implement functions in another fragments */
public abstract class FragmentBase extends Fragment {
  private static final String TAG = "TAG ".concat(FragmentBase.class.getSimpleName());

  /**
   * Function to manage response from observer in model views
   *
   * @param apiResponse Response from the api.
   */
  protected abstract void getResults(ApiResponse apiResponse);

  /**
   * Manage errors
   *
   * @param apiResponse Response from the api.
   * @return true if it has errors, false in otherwise
   */
  public boolean checkErrors(ApiResponse apiResponse) {
    if (apiResponse != null) {
      if (apiResponse.getErrorResponse().getErrorStandard() != null) {
        Log.e(
            TAG,
            "getResults: getErrorStandard "
                + apiResponse.getErrorResponse().getErrorStandard().getStatus());
        Bundle args = new Bundle();
        args.putString(
            ARG_PARAM1, apiResponse.getErrorResponse().getErrorStandard().getStatus() + "");
        args.putString(ARG_PARAM2, apiResponse.getErrorResponse().getErrorStandard().getMessage());
        apiResponse.setErrorResponse(new ErrorResponse());
        //
        // NavHostFragment.findNavController(this).navigate(R.id.action_global_errorFragment, args);
        return true;
      }
      if (apiResponse.getErrorResponse().getError() != null) {
        Log.e(
            TAG, "getResults: getError " + (apiResponse.getErrorResponse().getError().toString()));
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, apiResponse.getErrorResponse().getError().getMessage() + "");
        args.putString(ARG_PARAM2, apiResponse.getErrorResponse().getError().getLocalizedMessage());
        NavHostFragment.findNavController(this).navigate(R.id.action_global_errorFragment, args);
        return true;
      }
      return false;
    } else {
      Log.e(TAG, "checkErrors: null response", new NullPointerException());
      NavHostFragment.findNavController(this).navigate(R.id.action_global_errorFragment);

      return true;
    }
  }
}
