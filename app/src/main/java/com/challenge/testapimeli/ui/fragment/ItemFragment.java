package com.challenge.testapimeli.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import com.bumptech.glide.Glide;
import com.challenge.testapimeli.R;
import com.challenge.testapimeli.core.models.response.Product;
import com.challenge.testapimeli.data.request.ApiRequestDescription;
import com.challenge.testapimeli.data.request.ApiRequestItems;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseDescription;
import com.challenge.testapimeli.data.response.ApiResponseItem;
import com.challenge.testapimeli.databinding.FragmentItemBinding;
import com.challenge.testapimeli.ui.model.ItemViewModel;
import com.challenge.testapimeli.ui.model.factory.FactoryViewModelItem;
import com.google.gson.JsonElement;
import dagger.hilt.android.AndroidEntryPoint;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.inject.Inject;

/** Fragment to show the product with photos and description */
@AndroidEntryPoint
public class ItemFragment extends FragmentBase {

  private static final String TAG = "TAG ".concat(ItemFragment.class.getSimpleName());
  private ItemViewModel mViewModel;
  private FragmentItemBinding binding;
  public static final String IMAGES_LIST = "images_list";
  @Inject FactoryViewModelItem factoryViewModelItem;

  public ItemFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mViewModel = new ViewModelProvider(this, factoryViewModelItem).get(ItemViewModel.class);
    mViewModel.getItemsResult().observe(this, this::getResults);
    mViewModel.getItemDescriptionResult().observe(this, this::getDescriptionResult);
  }

  private void getDescriptionResult(@NonNull ApiResponse apiResponse) {
    if (apiResponse.getErrorResponse().getError() != null) {
      return;
    }
    ApiResponseDescription description = (ApiResponseDescription) apiResponse;
    if (description.getPlainText() != null && !description.getPlainText().isEmpty()) {
      binding.textDescription.setText(description.getPlainText());
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    binding = FragmentItemBinding.inflate(inflater, container, false);

    if (getArguments() != null && !mViewModel.hasItemsError()) {
      String idProduct = getArguments().getString(SearchFragment.ID_PRODUCT);
      ApiRequestItems search = new ApiRequestItems(idProduct, null);
      ApiRequestDescription description = new ApiRequestDescription(idProduct);
      mViewModel.callItems(search);
      mViewModel.callItemDescription(description);

    } else {
      NavHostFragment.findNavController(this).popBackStack();
    }
    return binding.getRoot();
  }

  @Override
  protected void getResults(ApiResponse apiResponse) {
    if (this.checkErrors(apiResponse)) {
      Log.e(TAG, "getResults: error ");
      return;
    }

    ApiResponseItem items = (ApiResponseItem) apiResponse;
    Product product = items.get(0).getProduct();
    if (product == null) {
      return;
    }
    binding.title.setText(product.getTitle());
    binding.secundaryInfo.setText(
        MessageFormat.format("Costo {0} {1}", product.getCurrencyId(), product.getPrice()));
    String url = "";
    ArrayList<String> list = new ArrayList<>();
    if (product.getPictures() != null) {
      for (JsonElement e : product.getPictures()) {
        url = e.getAsJsonObject().get("secure_url").getAsString();
        list.add(url);
        Log.d(TAG, "getResults: url " + url);
      }
    }
    Glide.with(this).load(url).fitCenter().into(binding.image);

    Bundle args = new Bundle();
    args.putStringArrayList(IMAGES_LIST, list);
    binding.image.setOnClickListener(
        v ->
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_infoFragment_to_imagesFragment, args));
  }
}
