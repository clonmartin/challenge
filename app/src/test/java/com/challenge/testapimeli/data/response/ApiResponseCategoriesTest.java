package com.challenge.testapimeli.data.response;

import static org.junit.Assert.*;

import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.core.models.response.Category;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.RetrofitMockService;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class ApiResponseCategoriesTest {

  private RetrofitMockService.MockApiService mockApiService;

  @Before
  public void setUp() {

    // Create a very simple Retrofit adapter which points the GitHub API.
    Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.SERVER_PATH).build();

    // Create a MockRetrofit object with a NetworkBehavior which manages the fake behavior of calls.
    NetworkBehavior behavior = NetworkBehavior.create();
    MockRetrofit mockRetrofit =
        new MockRetrofit.Builder(retrofit).networkBehavior(behavior).build();

    BehaviorDelegate<ApiService> delegate = mockRetrofit.create(ApiService.class);
    mockApiService = new RetrofitMockService.MockApiService(delegate, false);
    mockApiService.addCategories(StringTest.JSON_CATEGORY);
  }

  @Test
  public void getErrorResponse() {
    assertNotNull(new ApiResponseCategories().getErrorResponse());
  }

  @Test
  public void checkResponse() throws IOException {
    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();
    Assert.assertTrue(apiResponseCategoriesCall.execute().body().size() > 0);
  }

  @Test
  public void checkIds() throws IOException {
    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();
    ApiResponseCategories body = apiResponseCategoriesCall.execute().body();
    for (Category category : body) {
      assertNotNull(category.getId());
    }
  }

  @Test
  public void checkNames() throws IOException {
    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();
    for (Category category : apiResponseCategoriesCall.execute().body()) {
      assertNotNull(category.getName());
    }
  }
}
