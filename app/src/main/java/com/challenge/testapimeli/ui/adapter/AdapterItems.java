package com.challenge.testapimeli.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.challenge.testapimeli.R;
import com.challenge.testapimeli.core.models.response.Product;
import com.challenge.testapimeli.databinding.LayoutItemSearchBinding;
import dagger.hilt.android.qualifiers.ActivityContext;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/** Adepter to show all items in the searching fragment */
public class AdapterItems extends RecyclerView.Adapter<AdapterItems.ViewHolder>
    implements View.OnClickListener {

  private final List<Product> productList;
  private View.OnClickListener listener;
  private final Context context;

  @Inject
  public AdapterItems(@NonNull @ActivityContext Context context) {
    productList = new ArrayList<>();
    this.context = context;
  }

  public void addProductListItems(List<Product> newProductList) {
    productList.addAll(newProductList);
    this.notifyItemRangeInserted(productList.size() + 1, newProductList.size());
  }

  public List<Product> getProductList() {
    return productList;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutItemSearchBinding view =
        LayoutItemSearchBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
    view.getRoot().setOnClickListener(this);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
    holder.mIdView.setText(productList.get(position).getTitle());
    holder.mContentView.setText(
        MessageFormat.format(
            "Precio: {0} ${1}",
            productList.get(position).getCurrencyId(), productList.get(position).getPrice()));
    URL url = productList.get(position).getThumbnail();
    String stringUrl = "";
    if (url != null) stringUrl = url.toString();
    Glide.with(context)
        .load(stringUrl)
        .placeholder(R.drawable.vector_mercadolibre)
        .fitCenter()
        .into(holder.mImageProduct);
  }

  @Override
  public int getItemCount() {
    return productList.size();
  }

  public void setOnClickListener(@NonNull View.OnClickListener listener) {
    this.listener = listener;
  }

  @Override
  public void onClick(@NonNull View view) {
    if (listener != null) {
      listener.onClick(view);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final TextView mIdView;
    public final TextView mContentView;
    public final ImageView mImageProduct;

    public ViewHolder(@NonNull LayoutItemSearchBinding binding) {
      super(binding.getRoot());
      mImageProduct = binding.image;
      mIdView = binding.title;
      mContentView = binding.textPrice;
    }

    @NonNull
    @Override
    public String toString() {
      return super.toString() + " '" + mContentView.getText() + "'";
    }
  }
}
