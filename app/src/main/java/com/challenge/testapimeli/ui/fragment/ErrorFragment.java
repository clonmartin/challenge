package com.challenge.testapimeli.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.challenge.testapimeli.databinding.FragmentErrorBinding;
import java.text.MessageFormat;

/** Fragment to show errors and failures */
public class ErrorFragment extends Fragment {

  static final String ARG_PARAM1 = "param1";
  static final String ARG_PARAM2 = "param2";
  private static final String TAG = "TAG ".concat(ErrorFragment.class.getSimpleName());

  private String mParam1;
  private String mParam2;

  public ErrorFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
      Log.d(TAG, MessageFormat.format("{0} {1} {2}", "onCreate: ", mParam1, mParam2));
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    FragmentErrorBinding binding = FragmentErrorBinding.inflate(inflater, container, false);
    if (mParam1 != null && mParam2 != null) {
      binding.info.setText(MessageFormat.format("Error: {0} {1}", mParam1, mParam2));
    }

    return binding.getRoot();
  }
}
