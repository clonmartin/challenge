package com.challenge.testapimeli;

import static org.junit.Assert.*;

import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import com.challenge.testapimeli.core.models.error.ErrorStandard;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ErrorResponse;
import com.challenge.testapimeli.ui.fragment.FragmentBase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class FragmentBaseTest {
  FragmentBase fragmentBase;

  @Before
  public void setUp() {
    fragmentBase =
        new FragmentBase() {
          @Override
          protected void getResults(ApiResponse apiResponse) {}
        };
  }

  @Test
  public void checkErrors() {

    ErrorResponse errorResponse = new ErrorResponse();
    ApiResponse apiResponse =
        new ApiResponse() {
          @NonNull
          @Override
          public ErrorResponse getErrorResponse() {
            return errorResponse;
          }

          @Override
          public void setErrorResponse(ErrorResponse errorResponse) {}
        };
    assertFalse(fragmentBase.checkErrors(apiResponse));
    errorResponse.setErrorStandard(new ErrorStandard());
    assertTrue(fragmentBase.checkErrors(apiResponse));
    errorResponse.setError(new Exception());
    assertTrue(fragmentBase.checkErrors(apiResponse));
  }
}
