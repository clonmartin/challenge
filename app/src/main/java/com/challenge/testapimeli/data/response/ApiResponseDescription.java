package com.challenge.testapimeli.data.response;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.SerializedName;

/** Class for response in callback description */
public class ApiResponseDescription implements ApiResponse {
  public static final long serialVersionUID = 43287434328752L;
  private ErrorResponse errorResponse = new ErrorResponse();

  @NonNull
  @Override
  public ErrorResponse getErrorResponse() {
    return errorResponse;
  }

  // If the response is an error then set the error
  @Override
  public void setErrorResponse(ErrorResponse errorResponse) {
    this.errorResponse = errorResponse;
  }

  @SerializedName("plain_text")
  @Nullable
  private String plainText;

  @Nullable
  public String getPlainText() {
    return plainText;
  }

  public void setPlainText(@Nullable String plainText) {
    this.plainText = plainText;
  }
}
