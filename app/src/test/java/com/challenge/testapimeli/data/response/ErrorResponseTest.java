package com.challenge.testapimeli.data.response;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.challenge.testapimeli.BuildConfig;
import com.challenge.testapimeli.core.models.error.ErrorStandard;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.RetrofitMockService;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class ErrorResponseTest {
  private RetrofitMockService.MockApiService mockApiService;

  @Before
  public void setUp() {
    // Create a very simple Retrofit adapter which points the GitHub API.
    Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.SERVER_PATH).build();

    // Create a MockRetrofit object with a NetworkBehavior which manages the fake behavior of calls.
    NetworkBehavior behavior = NetworkBehavior.create();
    MockRetrofit mockRetrofit =
        new MockRetrofit.Builder(retrofit).networkBehavior(behavior).build();

    BehaviorDelegate<ApiService> delegate = mockRetrofit.create(ApiService.class);
    mockApiService = new RetrofitMockService.MockApiService(delegate, true);
  }

  @Test
  public void jsonToErrorResponseCheckStandardError() throws IOException {
    mockApiService.jsonError = StringTest.JSON_ERROR;

    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();
    ErrorStandard errorStandard =
        ErrorResponse.jsonToErrorResponse(apiResponseCategoriesCall.execute().errorBody());
    assertNotNull(errorStandard.getError());
    assertTrue(errorStandard.getStatus() > 0);
    assertNotNull(errorStandard.getMessage());
    assertNotNull(errorStandard.getCause());
  }

  @Test(expected = JsonSyntaxException.class)
  public void jsonToErrorResponseCheckExceptionMalformed() throws IOException {

    mockApiService.jsonError = (StringTest.JSON_ERROR_SYNTAX_1);
    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();

    ErrorStandard errorStandard =
        ErrorResponse.jsonToErrorResponse(apiResponseCategoriesCall.execute().errorBody());
    System.out.println(errorStandard.getMessage());
  }

  @Test(expected = JsonSyntaxException.class)
  public void jsonToErrorResponseCheckExceptionParsing() throws IOException {

    mockApiService.jsonError = (StringTest.JSON_ERROR_SYNTAX_2);
    Call<ApiResponseCategories> apiResponseCategoriesCall = mockApiService.callCategories();

    ErrorStandard errorStandard =
        ErrorResponse.jsonToErrorResponse(apiResponseCategoriesCall.execute().errorBody());
    System.out.println(errorStandard.getMessage());
  }
}
