package com.challenge.testapimeli.data.api.callback;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.data.response.ApiResponseItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/** Callback to get a particular item product. It can get one or more items. */
public class CallbackItems implements Callback<ApiResponseItem>, CallbackHelperError {
  private static final String TAG = "TAG ".concat(CallbackItems.class.getSimpleName());

  // Mutable live data to storage and set api response
  // If response fails then we use fail response to set the error response
  private final transient MutableLiveData<ApiResponse> liveData;

  public CallbackItems(MutableLiveData<ApiResponse> liveData) {
    this.liveData = liveData;
  }

  @Override
  public void onResponse(
      @NonNull Call<ApiResponseItem> call, @NonNull Response<ApiResponseItem> response) {
    if (response.isSuccessful() && response.body() != null) {
      ApiResponseItem apiResponseItem = response.body();
      Log.d(TAG, "onResponse: is success. Items:" + apiResponseItem.size());
      apiResponseItem.getErrorResponse().setError(null);
      liveData.setValue(apiResponseItem);
    } else {
      Log.d(TAG, "onResponse: fail");
      if (response.errorBody() != null) {
        ApiResponse bodyError = getBodyError(response.errorBody());
        liveData.setValue(bodyError);
        return;
      }
      ApiResponse failResponse = new ApiResponseCategories();
      failResponse.getErrorResponse().setError(new Exception());
      liveData.setValue(failResponse);
    }
  }

  @Override
  public void onFailure(@NonNull Call<ApiResponseItem> call, @NonNull Throwable t) {
    ApiResponse failResponse = new ApiResponseCategories();
    failResponse.getErrorResponse().setError(t);
    Log.e(TAG, "onFailure: ".concat(t.toString()), t);
    liveData.setValue(failResponse);
  }
}
