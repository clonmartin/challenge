package com.challenge.testapimeli.data.request;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import javax.inject.Inject;

/** Implementation class to create parameters to api search products */
public class ApiRequestSearch implements ApiRequest {
  @Nullable protected String query;
  protected int limit;
  protected int offset;

  /**
   * Set query to get products
   *
   * @param query Name of the product tha you are searching
   * @param limit Int number to limit the search
   * @param offset Int number to get a part of products from the total result.
   */
  @Inject
  public ApiRequestSearch(@Nullable String query, int limit, int offset) {
    this.query = query;
    this.limit = limit;
    this.offset = offset;
  }

  public void setQuery(@NonNull String query) {
    this.query = query;
  }

  @Nullable
  public String getQuery() {
    return query;
  }

  public int getLimit() {
    return limit;
  }

  public int getOffset() {
    return offset;
  }
}
