package com.challenge.testapimeli.ui.model.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.challenge.testapimeli.data.api.ApiManagerCategories;
import com.challenge.testapimeli.ui.model.MainViewModel;
import javax.inject.Inject;

/** Factory to create view model category */
@SuppressWarnings("unchecked")
public class FactoryViewModelCategory implements ViewModelProvider.Factory {
  private final ApiManagerCategories managerCategories;

  @Inject
  public FactoryViewModelCategory(ApiManagerCategories managerCategories) {
    this.managerCategories = managerCategories;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

    MainViewModel mainViewModel = new MainViewModel(managerCategories);

    if (modelClass.isAssignableFrom(mainViewModel.getClass())) {
      return (T) mainViewModel;
    }
    throw new IllegalArgumentException("Unknown ViewModel class");
  }
}
