package com.challenge.testapimeli.data.response;

import androidx.annotation.NonNull;

/** Interface to create api response classes */
public interface ApiResponse {

  /**
   * Set the error. If we don't have errors then it must be empty
   *
   * @return Error from api response
   */
  @NonNull
  ErrorResponse getErrorResponse();

  void setErrorResponse(ErrorResponse errorResponse);
}
