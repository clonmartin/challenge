package com.challenge.testapimeli.data;

import androidx.annotation.NonNull;
import com.challenge.testapimeli.data.response.ApiResponseCategories;
import com.challenge.testapimeli.data.response.ApiResponseDescription;
import com.challenge.testapimeli.data.response.ApiResponseItem;
import com.challenge.testapimeli.data.response.ApiResponseSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/** Retrofit service */
public interface ApiService {

  /**
   * Get list of categories
   *
   * @return List of categories
   */
  @GET("/sites/MLC/categories")
  Call<ApiResponseCategories> callCategories();

  /**
   * Get list of search
   *
   * @param searchProduct Product to search
   * @param limit Limit of products. Must be greater than 0
   * @param offset Offset to get next products
   * @return List of products
   */
  @GET("/sites/MLC/search")
  Call<ApiResponseSearch> callSearchProduct(
      @NonNull @Query("q") String searchProduct,
      @Query("limit") int limit,
      @Query("offset") int offset);

  /**
   * Get info by every id product
   *
   * @param ids Id product string
   * @param attributes Attributes of the products. i.e. id,price. It will bring id and price
   * @return Product info
   */
  @GET("https://api.mercadolibre.com/items")
  Call<ApiResponseItem> callItemAttributes(
      @NonNull @Query("ids") String ids, @NonNull @Query("attributes") String attributes);

  /**
   * Retrieve item description
   *
   * @param idItem Id item product
   * @return Product description
   */
  @GET("/items/{id}/description")
  Call<ApiResponseDescription> callDescription(@NonNull @Path("id") String idItem);
}
