package com.challenge.testapimeli.core.models.response;

import java.io.Serializable;

/** Properties of category item */
public class Category extends BasicAttributes implements Serializable {
  public static final long serialVersionUID = 43287434328745L;
}
