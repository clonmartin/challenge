package com.challenge.testapimeli.data.di;

import com.challenge.testapimeli.data.request.ApiRequestSearch;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;

/** Module to initialize different objects to inject in fragments */
@Module
@InstallIn(FragmentComponent.class)
public class UtilsModule {

  /**
   * Request to search products
   *
   * @return ApiRequestSearch for call api
   */
  @Provides
  public ApiRequestSearch requestSearch() {
    return new ApiRequestSearch(null, 50, 0);
  }
}
