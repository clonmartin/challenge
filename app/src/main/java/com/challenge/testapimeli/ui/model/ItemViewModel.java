package com.challenge.testapimeli.ui.model;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.challenge.testapimeli.data.api.ApiManagerDescription;
import com.challenge.testapimeli.data.api.ApiManagerItems;
import com.challenge.testapimeli.data.request.ApiRequestDescription;
import com.challenge.testapimeli.data.request.ApiRequestItems;
import com.challenge.testapimeli.data.response.ApiResponse;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

/** View model for Fragment item product. It shows detail product */
@HiltViewModel
public class ItemViewModel extends ViewModel {
  @NonNull private final LiveData<ApiResponse> itemsResult;
  @NonNull private final ApiManagerItems apiManagerItems;
  @NonNull private final LiveData<ApiResponse> descriptionResult;
  @NonNull private final ApiManagerDescription apiManagerDescription;

  /**
   * This view model uses api manager to send request and get responses setting descriptionResult
   * and itemsResult from callbacks
   *
   * @param apiManagerItems
   * @param apiManagerDescription
   */
  @Inject
  public ItemViewModel(
      @NonNull ApiManagerItems apiManagerItems,
      @NonNull ApiManagerDescription apiManagerDescription) {
    this.apiManagerItems = apiManagerItems;
    this.itemsResult = apiManagerItems.getResponseMutableLiveData();
    this.apiManagerDescription = apiManagerDescription;
    this.descriptionResult = apiManagerDescription.getResponseMutableLiveData();
  }

  public void callItems(@NonNull ApiRequestItems search) {
    this.apiManagerItems.getCallFromApi(search);
  }

  public void callItemDescription(@NonNull ApiRequestDescription idProduct) {
    this.apiManagerDescription.getCallFromApi(idProduct);
  }

  @NonNull
  public LiveData<ApiResponse> getItemsResult() {
    return itemsResult;
  }

  @NonNull
  public LiveData<ApiResponse> getItemDescriptionResult() {
    return descriptionResult;
  }

  /**
   * Check if response has an error
   *
   * @return True if it has an error
   */
  public boolean hasItemsError() {
    ApiResponse value = getItemsResult().getValue();
    return value != null && value.getErrorResponse().getError() != null;
  }

  /**
   * Check if response has an error
   *
   * @return True if it has an error
   */
  public boolean hasDescriptionError() {
    ApiResponse value = getItemDescriptionResult().getValue();
    return value != null && value.getErrorResponse().getError() != null;
  }
}
