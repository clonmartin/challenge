package com.challenge.testapimeli.data.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import com.challenge.testapimeli.data.ApiService;
import com.challenge.testapimeli.data.api.callback.CallbackItems;
import com.challenge.testapimeli.data.request.ApiRequest;
import com.challenge.testapimeli.data.request.ApiRequestItems;
import com.challenge.testapimeli.data.response.ApiResponse;
import com.challenge.testapimeli.data.response.ApiResponseItem;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;

/** Manage data api items */
public class ApiManagerItems implements ApiManager {
  private final MutableLiveData<ApiResponse> responseMutableLiveData;
  private final ApiService apiService;
  private final Callback<ApiResponseItem> responseItemCallback;

  /**
   * It sends a mutable live data to set into callback when the api responses
   *
   * @param apiService Api service interface
   */
  @Inject
  public ApiManagerItems(@NonNull ApiService apiService) {
    this.responseMutableLiveData = new MutableLiveData<>();
    this.apiService = apiService;
    this.responseItemCallback = new CallbackItems(responseMutableLiveData);
  }

  /**
   * Mutable live data
   *
   * @return A mutable live data set from callback response
   */
  @Override
  @NonNull
  public MutableLiveData<ApiResponse> getResponseMutableLiveData() {
    return responseMutableLiveData;
  }

  /** Create call to get items */
  @Override
  public void getCallFromApi(@Nullable ApiRequest apiRequest) {
    if (apiRequest != null) {
      ApiRequestItems requestItems = (ApiRequestItems) apiRequest;
      Call<ApiResponseItem> responseItemCall =
          apiService.callItemAttributes(requestItems.getId(), requestItems.getAttributes());

      responseItemCall.enqueue(responseItemCallback);
    }
  }
}
